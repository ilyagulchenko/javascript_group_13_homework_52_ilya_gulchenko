import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() rank = '';
  @Input() suit = '';

  getClassname() {
    return 'card rank-' + this.rank.toLowerCase() + ' ' + this.suit;
  }

  getSuit() {
    if (this.suit === 'spades') return '♠';
    if (this.suit === 'clubs') return '♣';
    if (this.suit === 'hearts') return '♥';
    else return '♦'
  }
}
