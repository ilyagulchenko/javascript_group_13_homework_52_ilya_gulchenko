import {Component} from '@angular/core';
import {CardDeck} from "../lib/CardDesk";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cardDeck = new CardDeck();
  cards = this.cardDeck.getCards(2);

  getAnotherCard() {
    this.cards.push(this.cardDeck.getCard());
  }

  getReset() {
    this.cardDeck = new CardDeck();
    this.cards = this.cardDeck.getCards(2);
  }

  getScore() {
    let score = 0;
    for (let i = 0; i < this.cards.length; i++) {
      score += this.cards[i].getScore();
    }
    return score;
  }

  // @ts-ignore
  getResult() {
    if (this.getScore() === 21) {
      return 'You won!';
    } else if (this.getScore() > 21) {
      return "You've lost!";
    }
  }
}
