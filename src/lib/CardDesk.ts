const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const SUITS = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  constructor(
    public rank: string,
    public suit: string
  ) {}

  getScore() {
    if (this.rank === 'J' || this.rank === 'Q' || this.rank === 'K') {
      return 10;
    } if (this.rank === 'A') {
      return 11;
    } else {
      return parseInt(this.rank);
    }
  }
}

export class CardDeck{
  cards: Card[] = [];

  constructor() {
    for (let i = 0; i < RANKS.length; i++) {
      for (let j = 0; j < SUITS.length; j++) {
        this.cards.push(new Card(RANKS[i], SUITS[j]));
      }
    }
  }

  getCard(): Card {
    let randomNumber = Math.floor(Math.random() * this.cards.length);
    const [card] = this.cards.splice(randomNumber, 1);
    return card;
  }

  getCards(howMany: number): Card[] {
    let cards: Card[] = [];
    for (let i = 0; i < howMany; i++) {
      cards.push(this.getCard());
    }
    return cards;
  }
}
